const readAll = require('./read-all');

const fileList = readAll('./');
fileList
  .then(returnData=>console.log(returnData))
  .catch(error=>console.error(error));
