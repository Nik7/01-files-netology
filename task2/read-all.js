const fs = require('fs');  

function readall (path) {
  const conf = {encoding: 'utf-8'};
  let returnData = [];
  return new Promise((done, fail) => {
    fs.readdir(path, (err, files) => {
    	if (err) {
    	  fail(err);
    	} else{
    	  //console.log(files);
    	  
    	  files.forEach((file, i) => {
    	  	fs.readFile(file, conf, (err, content) => {
    	     if (err) {
    	       fail(err);
    	     } else {
              returnData.push({file,content});
              if (i === (files.length-1)) {
                done(returnData);
              }
    	     }

    	    })
    	  	
    	  })
    	  
    	  	
    	}
    })
  })   

}
module.exports = readall;