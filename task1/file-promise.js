const fs = require('fs');

function read (filepath, data) {
const opts = {encoding:'utf-8'};
  return new Promise((done, fail) => {
    fs.readFile(filepath, opts, (err, data) => {
      if(err) {
      	fail(err);
      } else{
        done(data);
      }
    })      

  })
};

function write (filepath, data) {
const opts = {encoding:'utf-8'};
  return new Promise((done, fail) => {
    fs.writeFile(filepath, data, opts, err => {
      if(err) {
      	fail(err);
      } else{
        done(filepath);
      }
    })      

  })


}

module.exports = {
	read,
	write
}