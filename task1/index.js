const file = require('./file-promise');

let filedata;
file.read('./data.txt', filedata)
  .then(filedata => filedata.toUpperCase())
  .then(result => file.write('./data-promise.txt', result))
  .catch(error => console.error(error));
